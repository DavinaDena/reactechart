

import * as echarts from 'echarts';
import { ReactECharts } from './reactEchart';
import React from "react";


function App() {
  const data = {
    CORE: [
      {
        id: 103,
        name: "Energie des bâtiments et machines",
        scope: "CORE",
        tCo2: 9433.56
      },
      {
        id: 121,
        name: "Utilisation véhicules opérés par l'entreprise",
        scope: "CORE",
        tCo2: 7187.34
      }
    ],
    DOWNSTREAM: [
      {
        id: 114,
        name: "Fin de vie des produits vendus ou loués",
        scope: "DOWNSTREAM",
        tCo2: 18700
      },
      {
        id: 114,
        name: "Fin de vie des produits vendus ou loués",
        scope: "DOWNSTREAM",
        tCo2: 18700
      }
    ],
    UPSTREAM: [
      {
        id: 102,
        name: "Fret amont",
        scope: "UPSTREAM",
        tCo2: 12391.64
      },
      {
        id: 117,
        name: "Futurs emballages",
        scope: "UPSTREAM",
        tCo2: 7803
      },
      {
        id: 106,
        name: "Déplacements professionnels",
        scope: "UPSTREAM",
        tCo2: 5528.83
      }
    ]
  }

  const groupBy = (x, f) => x.reduce((a, b) => ((a[f(b)] ||= []).push(b), a), {});
  const coreByT = groupBy(data.CORE, v => v.tCo2)
  const downByT = groupBy(data.DOWNSTREAM, v => v.tCo2)
  const upByT = groupBy(data.UPSTREAM, v => v.tCo2)
  console.log(
    "core => ",
    Object.keys(coreByT),
    "down => ",
    Object.keys(downByT),
    "up => ",
    Object.keys(upByT)
  );



  const option = {
    tooltip: {
      trigger: 'axis',
      axisPointer: {
        // Use axis to trigger tooltip
        type: 'shadow' // 'shadow' as default; can also be 'line' or 'shadow'
      }
    },
    legend: {},
    grid: {
      left: '3%',
      right: '4%',
      bottom: '3%',
      containLabel: true
    },
    xAxis: {
      type: 'value'
    },
    yAxis: {
      type: 'category',
      data: ['CORE', 'DOWNSTREAM', 'UPSTREAM']
    },
    series: [
      {
        name: 'Direct',
        type: 'bar',
        stack: 'total',
        label: {
          show: true
        },
        emphasis: {
          focus: 'series'
        },
        data: Object.keys(coreByT).map(item =>{
          return{
            value: item
          }
        }
          )
      },
      {
        name: 'Mail Ad',
        type: 'bar',
        stack: 'total',
        label: {
          show: true
        },
        emphasis: {
          focus: 'series'
        },
        data: Object.keys(downByT).map(item =>{
          return{
            value: item
          }
        }
          )
      },
      {
        name: 'Affiliate Ad',
        type: 'bar',
        stack: 'total',
        label: {
          show: true
        },
        emphasis: {
          focus: 'series'
        },
        data: Object.keys(upByT).map(item =>{
          return{
            value: item
          }
        }
          )
      }
    ]
  };
  return (
    <div
      style={{

        height: "100vh",
        width: 'auto',
        overflow: "hidden"
      }}
    >
      <ReactECharts
        option={option}// use svg to render the chart.
      />
    </div>
  );
}

export default App;
